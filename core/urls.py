from django.urls import path
from core import views
from .models import *



app_name ="core"
urlpatterns = [
    ## Tecnicos
    path('list1/Tecincos/',views.List1.as_view(), name="list1"),
    path('detail/Tecnicos/<int:pk>/', views.DetailTecnico.as_view(), name= "detail1"),
    path('create/Tecnicos/',views.CreateTecnico.as_view(), name="create1"),
    path('update/Tecnicos/<int:pk>/', views.UpdateTecnicos.as_view(), name= "Update1"),
    path('delete/Tecnicos/<int:pk>/', views.DeleteTecnico.as_view(), name= "delete1"),
    ## Servicios
    path('list2/Servicios/',views.List2.as_view(), name="list2"),
    path('detail/Servicios/<int:pk>/', views.DetailServicio.as_view(), name= "detail2"),
    path('create/Servicios/',views.CreateServicio.as_view(), name="create2"),
    path('update/Servicios/<int:pk>/', views.UpdateServicios.as_view(), name= "Update2"),
    path('delete/Servicios/<int:pk>/', views.DeleteServicios.as_view(), name= "delete2"),
    ## Maquinas
    path('list3/Maquinas/',views.List3.as_view(), name="list3"),
    path('detail/Maquinas/<str:pk>/', views.DetailMaquinas.as_view(), name="detail3"),
    path('create/Maquinas/',views.CreateMaquina.as_view(), name="create3"),
    path('Update/Maquinas/<str:pk>/', views.UpdateMaquina.as_view(), name="Update3"),
    path('Delete/Maquinas/<str:pk>/', views.DeleteMaquina.as_view(), name="Delete3"),
]

